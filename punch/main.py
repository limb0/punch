# coding: utf-8

from __future__ import print_function

import argparse
import json
import os.path
import tempfile

from . import Processor


IGNORED_FILE_NAMES = {'.git'}


def get_path_from_url(url):
    return url.split('/')[-1].split('.')[0] if url else None


def _get_args():
    parser = argparse.ArgumentParser(description='Punch')
    parser.add_argument('-t', '--target',
                        help='URL of target Git repository.',
                        action='store')
    parser.add_argument('-s', '--source',
                        help='URL of source Git repository.',
                        action='store')
    parser.add_argument('path', nargs='?',
                        help='Path to the project\'s directory.',
                        action='store')

    return parser.parse_args()


def main():
    args = _get_args()

    pj_target_repo_url = args.target
    pj_source_repo_url = args.source

    pj_path = args.path or get_path_from_url(pj_target_repo_url) or get_path_from_url(pj_source_repo_url) or '.'

    if pj_target_repo_url:
        os.system('git clone {} {}'.format(pj_target_repo_url, pj_path))
    elif pj_path:
        try:
            os.mkdir('./{}'.format(pj_path))
        except OSError:
            pass
    os.chdir('./{}'.format(pj_path))

    if pj_source_repo_url:
        temp_dir_path = tempfile.mkdtemp()
        os.system('git clone {} {}'.format(pj_source_repo_url, temp_dir_path))
        os.system('rm -fr {}'.format(os.path.join(temp_dir_path, '.git')))
        os.system('cp -fr {} {}'.format(os.path.join(temp_dir_path, '*'), '.'))
        os.system('rm -fr {}'.format(temp_dir_path))

    with open('punch.txt', 'r') as f:
        params = json.load(f)

    print('-' * 32)
    print('Please fill in the parameters!')
    for param in params:
        param['value'] = raw_input('{}: '.format(param.get('description', param['name'])))
    print('-' * 32)

    params = {param['name']: param['value'] for param in params}

    processor = Processor(params, ignored_file_names=IGNORED_FILE_NAMES)
    processor.process_dir(os.getcwd())
