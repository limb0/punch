# coding: utf-8

from __future__ import print_function

import os.path
import random
import re
import string


class RandomChar(object):

    CHARS = string.ascii_letters + string.digits

    def __call__(self):
        return random.choice(self.CHARS)


class RandomString(object):

    def __call__(self, *args):
        length = 1
        if len(args) > 0:
            length = int(args[0])
        return ''.join(random.choice(RandomChar.CHARS) for _ in range(length))


class Processor(object):

    PARAM_REGEX = re.compile(r'__(\w*?)(\|([\w,]*?))?__', re.M)

    def __init__(self, params, ignored_file_names=None):
        self._params = params
        self._params['random_char'] = RandomChar()
        self._params['random_string'] = RandomString()

        self._ignored_file_names = ignored_file_names

    def process_text(self, text):
        match = self.PARAM_REGEX.search(text)
        if match is None:
            return text

        name = match.group(1)
        if name in self._params:
            value = self._params[name]
            if hasattr(value, '__call__'):
                args = match.group(3)
                args = args.split(',') if args is not None else []
                value = value(*args)
            return text[:match.start()] + value + self.process_text(text[match.end():])
        else:
            return text[:match.end()] + self.process_text(text[match.end():])

    def process_file(self, path):
        with open(path, 'r') as f:
            text = f.read()

        processed_text = self.process_text(text)
        if processed_text != text:
            with open(path, 'w') as f:
                f.write(processed_text)

    def process_dir(self, path):
        file_names = os.listdir(path)
        file_names = filter(lambda fn: fn not in self._ignored_file_names, file_names)

        subdir_paths = set()

        for file_name in file_names:
            processed_file_name = self.process_text(file_name)
            processed_file_path = os.path.join(path, processed_file_name)

            if processed_file_name != file_name:
                processed_file_path = os.path.join(path, processed_file_name)
                os.rename(os.path.join(path, file_name), processed_file_path)

            if os.path.isdir(processed_file_path):
                subdir_paths.add(processed_file_path)
            else:
                self.process_file(processed_file_path)

        for path in subdir_paths:
            self.process_dir(path)


from .main import main
