from setuptools import setup, find_packages


setup_params = {
    'entry_points': {
        'console_scripts': ['punch=punch:main'],
    }
}

setup(
    name='an-punch',
    version='0.0.4',
    packages=find_packages(),
    author='Dmitry Davidov',
    author_email='dmitrii.davidov@gmail.com',
    url='https://bitbucket.org/limb0/punch',
    license='MIT',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
    ],
    **setup_params
)
